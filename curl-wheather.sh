#!/bin/bash 
WEATHER=`curl -s https://tgftp.nws.noaa.gov/data/observations/metar/decoded/CYUL.TXT`;
TEMP="`echo "${WEATHER}" | grep 'Temperature' | cut -d "(" -f2 | cut -d " " -f1`";
SKY=`echo "${WEATHER}" | grep 'Sky conditions' | cut -d " " -f3-`;
WEA=`echo "${WEATHER}" | grep 'Weather' | cut -d " " -f2-`;
WIND=`echo "${WEATHER}" | grep "Wind:" | grep -o  "MPH.*" | cut  -d"(" -f2 | cut -d" " -f1`;
if [ -z "$TEMP" ]; then
	cat /dev/shm/weather.txt
	exit;
fi
TEMP="${TEMP}°C"
if [ -n "$SKY" ]; then
	TEMP="${TEMP}, ${SKY}"
fi
if [ -n "$WEA" ]; then
	TEMP="${TEMP}, ${WEA}"
fi
if [ -n "$WIND" ]; then
	TEMP="${TEMP}, ${WIND}kt"
fi
echo ${TEMP} > /dev/shm/weather.txt
echo ${TEMP}
